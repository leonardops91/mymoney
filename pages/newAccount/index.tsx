import Head from "next/head";
import Link from "next/link";
import React from "react";
import Input from "../../components/forms/input";
import useForm from "../../components/hooks/useForm";

export default function NewAccountPage() {
    const name = useForm('name')
    const surname = useForm()
    const birthday = useForm('birthday')
    const email = useForm('email')
    const password = useForm('password')
    const passwordConfirmation = useForm('passwordConfirmation')

    function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
    }

    return (
        <div>
            <Head>
                <title>Novo cadastro</title>
            </Head>
            <main>
                <h2>Criar uma nova conta</h2>
                <form onSubmit={e => handleSubmit(e)}>
                    <Input label='Nome' type='text' id='name' required={true} {...name} />

                    <Input label='Sobrenome' type='text' id='surname' required={false} {...surname} />

                    <Input label='Data de nascimento' type='date' id='birthday' required={true} {...birthday} />

                    <Input label='E-mail' type="email" id='email' required={true} {...email} />

                    <Input label='Senha' type="password" id='password' required={true} {...password} />
                    <Input label='Confirmar senha' type="password" id='passwordConfirmation' required={true} {...passwordConfirmation} />

                    <button>Enviar</button><br /><br />

                    <Link href="/"><a>Voltar ao login</a></Link>
                </form>
            </main>
        </div>
    )
}