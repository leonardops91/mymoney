import Head from "next/head";
import Link from "next/link";
import React from "react";
import Input from "../../components/forms/input";
import useForm from "../../components/hooks/useForm";

export default function PasswordRecovery() {
    const email = useForm('email')
    const birthday = useForm('birthday')

    function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        console.log('enviando...');

    }
    function handleClick() {
        console.log("voltando a tela inicial");

    }
    return (
        <>
            <Head>
                <title>Recuperar Senha</title>
            </Head>
            <main>
                <form onSubmit={e => handleSubmit(e)}>
                    <Input label='E-mail' type='email' id='email' required={true} {...email} />
                    <Input label='Data de nascimento' type='date' id='birthday' required={true} {...birthday} />
                    <button>Enviar</button><br />
                    <Link href='/'><a style={{ cursor: 'pointer' }} onClick={handleClick}>Voltar a tela de login</a></Link><br />
                </form>
            </main>
        </>
    )
}