import Head from 'next/head'
import Link from 'next/link'
import React from 'react'
import Input from '../components/forms/input'
import useForm from '../components/hooks/useForm'

export default function Home() {
  const user = useForm('user')
  const password = useForm('password')

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    console.log(user.value, password)
  }
  function handleClick() {
    console.log('clicou');

  }
  return (
    <>
      <Head>
        <title>MyMoney</title>
      </Head>

      <form onSubmit={e => handleSubmit(e)}>
        <Input label='Usuário' id="user" type="text" required={false} {...user} />
        <Input label='Senha' id="password" type="password" required={false} {...password} />
        <button style={{ cursor: 'pointer' }}>Entrar</button><br />
        <Link href="/newAccount" ><a onClick={handleClick}>Não estou cadastrado</a></Link><br />
        <Link href="/passwordRecovery"><a onClick={handleClick}>Não lembro minha senha</a></Link><br />
      </form>
    </>
  )
}
