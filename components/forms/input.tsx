import { ChangeEventHandler } from "react";

interface IInput { 
    id: string;
    type: string; 
    value: string, 
    label: string, 
    required: boolean, 
    onChange: ChangeEventHandler<HTMLInputElement>;
    onBlur: ChangeEventHandler<HTMLInputElement>; 
    error: string 
}

const Input = ({ id, type, value, label, required, onChange, onBlur, error }: IInput) : JSX.Element => {
    return (
        <>
            <label htmlFor={id}>{label}{required && <text style={{ color: 'red' }}>*</text>}:</label><br />
            <input type={type} name={id} id={id} value={value} onChange={onChange} onBlur={onBlur} /><br />
            {error && <small style={{ color: 'red', fontWeight: 'bold' }}>{error}</small>}
            <br />
        </>
    )
}

export default Input