import Head from "next/head"
import Link from "next/link"

const NewPassword = () => {
    return (
        <>
            <Head>
                <title>Mensagem enviada</title>
            </Head>
            <main>
                <h3>Uma mensagem foi enviada para seu e-mail. Siga as instruções para recuperação de senha.</h3>
                <Link href="/"><a>Retornar à página inicial</a></Link>
            </main>
        </>
    )
}

export default NewPassword