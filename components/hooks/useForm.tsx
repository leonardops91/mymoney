import { useState } from "react"
interface ITypes {
    [key: string]: {
        regex: any;
        message: string;
    }
}

const types: ITypes = {
    email: {
        regex: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        message: "Insira um e-mail válido"
    },
    birthday: {
        regex: /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/,
        message: "Insira uma data válida"
    },

}

const useForm = (type?: string) => {
    const [value, setValue] = useState<string>('')
    const [error, setError] = useState('')

    function validate(value: string) {
        if (!type) return true
        if (value.length === 0) {
            setError('Campo obrigatório')
            return false

        } else if (types[type] && !types[type].regex.test(value)) {
            setError(types[type].message)
            return false
        } else {
            setError('')
            return true
        }
    }

    function onChange({ target }: React.ChangeEvent<HTMLInputElement>) {

        if (error !== null) {
            validate(target.value)

        }
        setValue(target.value)
    }

    return (
        {
            value,
            onChange,
            error,
            onBlur: () => validate(value),
            validate: () => validate(value)
        }
    )
}

export default useForm